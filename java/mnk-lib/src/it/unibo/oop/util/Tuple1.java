package it.unibo.oop.util;

public interface Tuple1<A> extends Tuple {
    A getFirst();
}
